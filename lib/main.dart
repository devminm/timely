import 'package:flutter/material.dart';

import 'models/place_opening_hours.dart';
import 'models/places/bank.dart';
import 'models/places/bar.dart';
import 'models/places/cafe.dart';
import 'models/places/disco.dart';
import 'models/places/fiseur.dart';
import 'models/places/spielplatz.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.indigo,cardTheme: CardTheme(elevation : 2,shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12))),dialogTheme: DialogTheme(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8))),
          fontFamily: "Roboto"
      ),
      home: const MyHomePage(title: 'Timely'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  List<PlaceOpeningHours> places = [BankOpeningHours(),BarOpeningHours(),SpeilplatzOpeningHours(),CafeOpeningHours(),FriseurOpeningHours(),DiscoOpeningHours()];

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (notification){
          notification.disallowIndicator();
          return true;
        },
        child: ListView.builder(

            itemCount:  places.length,
            shrinkWrap: true,
            itemBuilder: (context,index){
              return PlaceWidget(place: places[index], showStatusDialog: (PlaceOpeningHours place,[when]) => showStatusDialog(place));

            }),
      ),
      floatingActionButton:  FloatingActionButton(
        onPressed: () async {
          String selectedPlace = 'Bank';
          int selectedDayOfWeek = 1;
          TimeOfDay selectedTime = const TimeOfDay(hour: 10, minute: 30);

          showDialog(context: context, builder: (ctx){
            return  StatefulBuilder(
              builder: (context,state){
                return AlertDialog(
                  title: const Text('Select Place, Day and Time'),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Dropdown for selecting place
                      const Text('Place'),
                      DropdownButton<String>(
                        value: selectedPlace,
                        onChanged: (value) {
                          state(() {
                            selectedPlace = value!;
                          });
                        },
                        items: places.map((e) =>  DropdownMenuItem(
                          value: e.name,
                          child: Text(e.name),
                        )).toList(),
                      ),
                      const SizedBox(height: 16),
                      // Dropdown for selecting day of week
                      const Text('Day of Week'),
                      DropdownButton<int>(
                        value: selectedDayOfWeek,
                        onChanged: (value) {
                          state(() {
                            selectedDayOfWeek = value!;
                          });
                        },
                        items: const [
                          DropdownMenuItem(
                            value: 1,
                            child: Text('Monday'),
                          ),
                          DropdownMenuItem(
                            value: 2,
                            child: Text('Tuesday'),
                          ),
                          DropdownMenuItem(
                            value: 3,
                            child: Text('Wednesday'),
                          ),
                          DropdownMenuItem(
                            value: 4,
                            child: Text('Thursday'),
                          ),
                          DropdownMenuItem(
                            value: 5,
                            child: Text('Friday'),
                          ),
                          DropdownMenuItem(
                            value: 6,
                            child: Text('Saturday'),
                          ),
                          DropdownMenuItem(
                            value: 7,
                            child: Text('Sunday'),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16),
                      // Time picker for selecting time of day
                      const Text('Time'),
                      InkWell(
                        onTap: () async {
                          TimeOfDay? time = await showTimePicker(
                            context: context,
                            initialTime: selectedTime,
                          );
                          if (time != null) {
                            state(() {
                              selectedTime = time;
                            });
                          }
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Colors.grey),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 12),
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              Text('${selectedTime.hour}:${selectedTime.minute.toString().padLeft(2, '0')}'),
                              const SizedBox(width: 8),
                              const Icon(Icons.access_time),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('CANCEL'),
                    ),
                    TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();

                        showStatusDialog(places.firstWhere((place) => place.name == selectedPlace),DateTime(2023,1,selectedDayOfWeek + 1,selectedTime.hour,selectedTime.minute));

                      },
                      child: const Text('OK'),
                    ),
                  ],
                );
              },
            );
          });

        },
        child: const Icon(Icons.calendar_month_outlined),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  void showStatusDialog(PlaceOpeningHours place, [DateTime? when]){
    showDialog(context: context, builder: (cxt){

      return AlertDialog(
        title: Row(mainAxisSize: MainAxisSize.max,
          children: [
            Text(place.name),
            const Spacer(),
            Container(decoration: BoxDecoration(color: place.isOpened(when) ? Colors.green.withOpacity(0.15):Colors.red.withOpacity(0.15),
                borderRadius: BorderRadius.circular(12)),
              child:  Padding(
                padding: const EdgeInsets.symmetric(vertical: 4.0,horizontal: 8),
                child: Text(place.isOpened(when) ? "Opened":"Closed",style: TextStyle(fontSize: 14,color: place.isOpened(when) ? Colors.green:Colors.red),),
              ),)              ],

        ),
        content: Column(mainAxisSize: MainAxisSize.min,
          children: [
            const Divider(color: Colors.grey),
            Container(decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12)),
              child:  Padding(
                padding: const EdgeInsets.symmetric(vertical: 4.0,horizontal: 8),
                child: Text(place.getNextStatus(when),style: Theme.of(context).textTheme.bodyMedium?.copyWith(color: Colors.indigo)),
              ),),
            const Divider(color: Colors.grey),

            Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                const Text('Opening hours:'),
                const SizedBox(height: 6,),

                for (var hours in place.hours ) ...[
                  if(hours.values.last.isNotEmpty)
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 2.0),
                      child: Row(
                        children: [
                          Text('${hours.keys.first.name.capitalize()}: ',style: Theme.of(context).textTheme.bodyMedium,),
                          for (var openingHours in hours.values) ...[
                            for(var hour in openingHours) ...[
                              Text('${hour.openHour}${hour.openMin==0?"":":${hour.openMin}"}-${hour.closeHour}${hour.closeMin==0?"":":${hour.closeMin}"} ',style: Theme.of(context).textTheme.bodyMedium),

                            ],
                          ],
                        ],
                      ),
                    ),
                ],
              ],
            ),
          ],
        ),
        actions: <Widget>[
          TextButton(
            child: const Text('OK'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      );
    });

  }
}

class PlaceWidget extends StatelessWidget {
  const PlaceWidget({
    super.key,
    required this.place, required this.showStatusDialog,
  });

  final PlaceOpeningHours place;
  final void Function(PlaceOpeningHours place,[DateTime? when]) showStatusDialog;

  @override
  Widget build(BuildContext context) {
    bool isOpened = place.isOpened();

    return Padding(
      padding: const EdgeInsets.all(4.0),
      child: Card(clipBehavior: Clip.antiAliasWithSaveLayer,
        child: ListTile(onTap: () => onPressed(context),contentPadding: const EdgeInsets.all(8),
          leading: Image.asset("assets/images/${place.name.toLowerCase()}.png",width: 50,height: 50,),
          title: Row(mainAxisSize: MainAxisSize.max,
            children: [
              Text(place.name),
              const Spacer(),
              Container(decoration: BoxDecoration(color: isOpened ? Colors.green.withOpacity(0.15):Colors.red.withOpacity(0.15),
                  borderRadius: BorderRadius.circular(12)),
                child:  Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0,horizontal: 8),
                  child: Text(isOpened ? "Opened":"Closed",style: TextStyle(fontSize: 14,color: isOpened ? Colors.green:Colors.red),),
                ),)
            ],
          ),
          subtitle: Column(crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(place.getNextStatus()),
              const SizedBox(height: 4,),
              Text(place.notes,style: Theme.of(context).textTheme.bodySmall,),


            ],
          ),
        ),),
    );
  }

  void onPressed(BuildContext context) {
    showStatusDialog(place);
  }
}


