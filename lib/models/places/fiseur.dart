import '../opening_hours.dart';
import '../place_opening_hours.dart';

class FriseurOpeningHours extends PlaceOpeningHours {
  FriseurOpeningHours() : super(
      3,
      name: 'Friseur',
      hours: [
        {Weekday.tuesday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 30)]},
        {Weekday.wednesday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 30)]},
        {Weekday.thursday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 30)]},
        {Weekday.friday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 30)]},
        {Weekday.saturday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 14,closeMin: 30)]},
        {Weekday.sunday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 14,closeMin: 30)]},
        {Weekday.monday:[]},
      ],
      notes: 'Closed on Mondays (Ruhetag)'
  );
}