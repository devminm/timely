import '../opening_hours.dart';
import '../place_opening_hours.dart';

class BankOpeningHours extends PlaceOpeningHours {
  BankOpeningHours() : super(
      1,
      name: 'Bank',
      hours: [
      {Weekday.monday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 13,closeMin: 0),OpeningHours(openHour: 14, openMin: 0, closeHour: 17,closeMin: 30),]},
      {Weekday.tuesday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 13,closeMin: 0),OpeningHours(openHour: 14, openMin: 0, closeHour: 17,closeMin: 30),]},
      {Weekday.wednesday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 13,closeMin: 0),OpeningHours(openHour: 14, openMin: 0, closeHour: 17,closeMin: 30),]},
      {Weekday.thursday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 13,closeMin: 0),OpeningHours(openHour: 14, openMin: 0, closeHour: 17,closeMin: 30),]},
      {Weekday.friday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 13,closeMin: 0),OpeningHours(openHour: 14, openMin: 0, closeHour: 17,closeMin: 30),]},
       {Weekday.saturday:[]},
      {Weekday.sunday:[]},
      ],
      notes: 'Closed on weekends'
  );
}