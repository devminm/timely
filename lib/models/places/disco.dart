import '../opening_hours.dart';
import '../place_opening_hours.dart';

class DiscoOpeningHours extends PlaceOpeningHours {
  DiscoOpeningHours() : super(
      2,
      name: 'Disco',
      hours: [
        {Weekday.monday:[OpeningHours(openHour: 20, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.tuesday:[OpeningHours(openHour: 20, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.wednesday:[OpeningHours(openHour: 20, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.thursday:[OpeningHours(openHour: 20, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.friday:[OpeningHours(openHour: 20, openMin: 0, closeHour: 3,closeMin: 0)]},
        {Weekday.saturday:[OpeningHours(openHour: 20, openMin: 0, closeHour: 3,closeMin: 0)]},
        {Weekday.sunday:[]},

      ],
      notes: 'Closed on Sundays'
  );
}