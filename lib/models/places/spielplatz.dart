import '../opening_hours.dart';
import '../place_opening_hours.dart';

class SpeilplatzOpeningHours extends PlaceOpeningHours {
  SpeilplatzOpeningHours() : super(
      4,
      name: 'Spielplatz',
      hours: [
        {Weekday.monday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.tuesday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.wednesday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.thursday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.friday:[OpeningHours(openHour: 7, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.saturday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 14,closeMin: 0)]},
        {Weekday.sunday:[OpeningHours(openHour: 8, openMin: 30, closeHour: 14,closeMin: 0)]},
      ],
      notes: 'Täglich geöffnet'
  );
}