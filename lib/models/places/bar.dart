import '../opening_hours.dart';
import '../place_opening_hours.dart';

class BarOpeningHours extends PlaceOpeningHours {
  BarOpeningHours() : super(
      6,
      name: 'Bar',
      hours: [
        {Weekday.monday:[OpeningHours(openHour: 8, openMin: 0, closeHour: 12,closeMin: 0),OpeningHours(openHour: 18, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.tuesday:[OpeningHours(openHour: 8, openMin: 0, closeHour: 12,closeMin: 0),OpeningHours(openHour: 18, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.wednesday:[OpeningHours(openHour: 8, openMin: 0, closeHour: 12,closeMin: 0),OpeningHours(openHour: 18, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.thursday:[OpeningHours(openHour: 8, openMin: 0, closeHour: 12,closeMin: 0),OpeningHours(openHour: 18, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.friday:[OpeningHours(openHour: 8, openMin: 0, closeHour: 12,closeMin: 0),OpeningHours(openHour: 18, openMin: 0, closeHour: 1,closeMin: 0)]},
        {Weekday.saturday:[OpeningHours(openHour: 8, openMin: 0, closeHour: 12,closeMin: 0),OpeningHours(openHour: 18, openMin: 0, closeHour: 3,closeMin: 0)]},
      ],
      notes: 'Täglich geöffnet'
  );
}