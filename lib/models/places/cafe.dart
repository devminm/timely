import '../opening_hours.dart';
import '../place_opening_hours.dart';

class CafeOpeningHours extends PlaceOpeningHours {
  CafeOpeningHours() : super(
      5,
      name: 'Cafe',
      hours: [
        {Weekday.tuesday:[OpeningHours(openHour: 9, openMin: 0, closeHour: 11,closeMin: 30),OpeningHours(openHour: 12, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.wednesday:[OpeningHours(openHour: 9, openMin: 0, closeHour: 11,closeMin: 30),OpeningHours(openHour: 12, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.thursday:[OpeningHours(openHour: 9, openMin: 0, closeHour: 11,closeMin: 30),OpeningHours(openHour: 12, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.friday:[OpeningHours(openHour: 9, openMin: 0, closeHour: 11,closeMin: 30),OpeningHours(openHour: 12, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.saturday:[OpeningHours(openHour: 9, openMin: 0, closeHour: 11,closeMin: 30),OpeningHours(openHour: 12, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.sunday:[OpeningHours(openHour: 9, openMin: 0, closeHour: 11,closeMin: 30),OpeningHours(openHour: 12, openMin: 30, closeHour: 18,closeMin: 0)]},
        {Weekday.monday:[]},
      ],
      notes: 'Mittagspause von 11:30 bis 12:30'
  );
}