
import 'dart:ffi';

import 'package:flutter/material.dart';

import 'opening_hours.dart';

class PlaceOpeningHours{
  final int id;
  final String name;
  final List<Map<Weekday, List<OpeningHours>>> hours; // map of day of week (1 = Monday) to opening hours
  final String notes;
  PlaceOpeningHours(this.id, {required this.name, required this.hours, required this.notes});

  String getNextStatus([DateTime? when]) {
    DateTime currentTime = DateTime.now();

    if (when != null) {

      currentTime = when;
    }
    // Determine the next opening time
    TimeOfDay now = TimeOfDay.fromDateTime(currentTime);

    for (var i = 0; i < 7; i++) { // iterate over next 7 days

      var weekday = Weekday.values[(currentTime.weekday + i - 1 ) % 7 ];

      Map<Weekday, List<OpeningHours>>? todayHours = hours.firstWhere((map) => map.containsKey(weekday),orElse: ()=>{});
      Map<Weekday, List<OpeningHours>>? yesterdayHours = hours.firstWhere((map) {return map.containsKey(Weekday.values[(currentTime.weekday - 1)]);});

      for (var hours in yesterdayHours[Weekday.values[currentTime.weekday - 1]]!) {
        var openTime = TimeOfDay(
            hour: hours.openHour, minute: hours.openMin);
        var closeTime = TimeOfDay(
            hour: hours.closeHour, minute: hours.closeMin);
        if (closeTime.value() <= openTime.value()) {
          if (now.value() <= closeTime.value()) {
            return 'Open until ${hours.closeHour}:${hours.closeMin
                .toString().padLeft(2, '0')}';
          }
        }
      }

      //check today's time slots
      for (var hours in todayHours[weekday]!) {
        var openTime = TimeOfDay(
            hour: hours.openHour, minute: hours.openMin);
        var closeTime = TimeOfDay(
            hour: hours.closeHour, minute: hours.closeMin);
        if (closeTime.value() < openTime.value()) {
          closeTime =
              TimeOfDay(hour: hours.closeHour + 24, minute: hours.closeMin);
        }

        if ( (now.value() <= openTime.value()) ||
            currentTime.weekday <= weekday.index || !isOpened(currentTime))  {
          // The place is not open yet
          return 'Next opening time: ${weekday.name.capitalize()} ${hours
              .openHour}:${hours.openMin.toString().padLeft(2, '0')}';
        } else if ((now.value() <= closeTime.value()) && isOpened(currentTime)) {
          // The place is open
          return 'Open until ${hours.closeHour}:${hours.closeMin.toString()
              .padLeft(2, '0')}';
        }
      }
    }


    // The place is closed for the next 7 days
    return 'Closed for the next 7 days';
  }

  bool isOpened([DateTime? when]) {
    var now = DateTime.now();

    if(when != null){
      now = when;
    }
    final weekday = Weekday.values[now.weekday - 1]; // get the current day of week
    final time = TimeOfDay.fromDateTime(now); // get the current time
    // try {
    Map<Weekday, List<OpeningHours>>? todayHours = hours.firstWhere((map) =>
        map.containsKey(weekday));
    Map<Weekday, List<OpeningHours>>? yesterdayHours = hours.firstWhere((
        map) {
      return map.containsKey(Weekday.values[(now.weekday - 1)]);
    });

    //check if some time slots are in yesterday
    for (var hours in yesterdayHours[Weekday.values[now.weekday - 1]]!) {
      var openTime = TimeOfDay(hour:hours.openHour,minute: hours.openMin);
      var closeTime = TimeOfDay(hour: hours.closeHour,minute: hours.closeMin);
      if(closeTime.value() <= openTime.value()){
        if(time.value() < closeTime.value()){
          return true;
        }
      }
    }

    //check today's time slots
    for (final openingHours in todayHours[weekday]!) {
      final openTime = TimeOfDay(hour: openingHours.openHour, minute: openingHours.openMin);
      var closeTime = TimeOfDay(hour: openingHours.closeHour, minute: openingHours.closeMin);
      if (closeTime.value() <= openTime.value()) {
        closeTime = TimeOfDay(hour: openingHours.closeHour + 24, minute: openingHours.closeMin);
      }
      if (time.value() >= openTime.value() && time.value() <= closeTime.value()) {
        return true;
      }
    }

    return false;
  }
}

enum Weekday{
  monday,
  tuesday,
  wednesday,
  thursday,
  friday,
  saturday,
  sunday
}
extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }
}

extension TimeOfDayExtension on TimeOfDay{
  double value(){
    return hour + minute/60;
  }
}