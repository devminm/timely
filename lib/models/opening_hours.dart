import 'package:flutter/material.dart';

class OpeningHours{
  final int openHour;
  final int openMin;
  final int closeHour;
  final int closeMin;

  OpeningHours({required this.openHour, required this.openMin, required this.closeHour, required this.closeMin});


}