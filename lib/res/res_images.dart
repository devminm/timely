class ImagesResources {
  static const String _imagesBasePath = 'assets/images/';

  static const String bank = _imagesBasePath + 'bank.png';
  static const String bar = _imagesBasePath + 'bar.png';
  static const String disco = _imagesBasePath + 'disco.png';
  static const String cafe = _imagesBasePath + 'cafe.png';
  static const String friseur = _imagesBasePath + 'friseur.png';
  static const String spielPlatz = _imagesBasePath + 'spielplatz.png';

}